let data = [];
const leftTable = document.getElementById('left-table');
const rightTable = document.getElementById('right-table');

// ...

document.getElementById('csvFileInput').addEventListener('change', function(e) {
    const file = e.target.files[0];
    if (file) {
        // Vérifier le type de fichier
        
        if (file.name.endsWith('.csv')){
            // Si ce n'est pas un fichier Excel (.xlsx), utilisez PapaParse pour les fichiers CSV
            Papa.parse(file, {
                header: true,
                complete: function(results) {
                    const csvData = results.data;

                    // Afficher les données dans la table de gauche
                    displayData(leftTable, csvData);
                }
            });
        }
        else if (file.name.endsWith('.xlsx')) {
            const reader = new FileReader();
            reader.onload = function(e) {
                const xlsxData = new Uint8Array(e.target.result);
                const workbook = XLSX.read(xlsxData, { type: 'array' });

                // Sélectionnez la première feuille de calcul du classeur
                const sheetName = workbook.SheetNames[0];
                const worksheet = workbook.Sheets[sheetName];

                // Convertir le contenu de la feuille de calcul en tableau de données
                const excelData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

                // Afficher les données dans la table de gauche
                displayData(leftTable, excelData);
            };

            reader.readAsArrayBuffer(file);
        };
    };
});

// ...


function displayData(table, data) {
    table.innerHTML = '';

    data.forEach(function(row, index) {
        const isLastRow = index === data.length - 1; // Vérifier si c'est la dernière ligne
        const tr = document.createElement('tr');
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';

        for (const key in row) {
            const td = document.createElement('td');
            td.textContent = row[key];
            tr.appendChild(td);
        }

        if (!isLastRow) { // Ajouter la case à cocher uniquement si ce n'est pas la dernière ligne
            const tdCheckbox = document.createElement('td');
            tdCheckbox.appendChild(checkbox);
            tr.appendChild(tdCheckbox);
            table.appendChild(tr);
        }
    });
}

document.getElementById('generateSummary').addEventListener('click', function() {
    const checkboxes = leftTable.querySelectorAll('input[type="checkbox"]');
    rightTable.innerHTML = '';

    checkboxes.forEach(function(checkbox) {
        if (checkbox.checked) {
            const row = checkbox.parentNode.parentNode;
            const clonedRow = row.cloneNode(true);
            rightTable.appendChild(clonedRow);
        }
    });
});

$("#btnPrint").on("click", function () {
    var checkedRows = rightTable.querySelectorAll('tr');
    var contentToPrint = '<html><head><title>Html to PDF</title></head><body>';
    
    // Ajouter la ligne de titre centrée en haut
    contentToPrint += '<div style="text-align: center; margin-bottom: 20px;"><h1>Final Table</h1></div>';
    
    // Créer une table centrée au milieu
    contentToPrint += '<div style="text-align: center;"><table style="border-collapse: collapse; margin: 0 auto;">';

    checkedRows.forEach(function(row, index) {
        const checkbox = row.querySelector('input[type="checkbox"]');
        if (checkbox && checkbox.checked) {
            checkbox.remove();
            contentToPrint += '<tr>';
            const cells = row.querySelectorAll('td');
            cells.forEach(function(cell, cellIndex) {
                contentToPrint += '<td style="border: 1px solid black;">' + cell.innerHTML + '</td>';
            });
            contentToPrint += '</tr>';
        }
    });

    contentToPrint += '</table></div>';
    contentToPrint += '</body></html>';
    
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write(contentToPrint);
    printWindow.document.close();
    printWindow.print();
});
